﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Unimatrix
{
    public class TextMeshAlign : MonoBehaviour
    {
        public Camera targetCamera;
        public float marginPowerX = 0.3f;
        public float marginPowerY = 0.5f;

        bool _inited;
        TextMeshPro _textMeshPro;

        void TryInit()
        {
            if (!_inited)
            {
                _inited = true;
            }
            else
            {
                return;
            }
            Init();
        }

        void Init()
        {
            _textMeshPro = GetComponent<TextMeshPro>();
        }

        private void Start()
        {
            TryInit();
        }

        void LateUpdate()
        {
            TryInit();

            float width = Screen.width;
            float height = Screen.height;
            float div = width / height;
            float size = targetCamera.orthographicSize;

            float xMargin = -div * size;
            float yMargin = -size;

            float marginX = marginPowerX * size;
            float marginY = marginPowerY * size;

            _textMeshPro.margin = new Vector4(xMargin + marginX, yMargin + marginPowerY, xMargin - marginX, yMargin + marginPowerY);
        }
    }

}
