﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

namespace Unimatrix
{
    public class TextMeshProducer : MonoBehaviour
    {
        public Camera targetCamera;
        public float textUpdatePeriod = 0.05f;
        public float regeneratedTextPercent = 0.4f;
        public int emptySpaceTextPercent = 8;

        TextMeshPro[] textMatrixes;

        int _currentMatrixIndex = 0;
        bool _inited;

        void TryInit()
        {
            if (!_inited)
            {
                _inited = true;
            }
            else
            {
                return;
            }
            Init();
        }

        void Init()
        {
            textMatrixes = GetComponentsInChildren<TextMeshPro>();
        }

        private void Start()
        {
            TryInit();
            Invoke("GenerateNewTextMatrix", 0f);
            InvokeRepeating("UpdateMatrix", textUpdatePeriod, textUpdatePeriod);
        }

        StringBuilder _stringBuilder = new StringBuilder();

        void GenerateNewTextMatrix()
        {
            _currentMatrixIndex = 0;
            _stringBuilder.Length = 0;
            float div = Screen.width / (float)Screen.height;
            int endlineCount = 0;
            const float X_SYMBOL_SIZE = 30f;
            float stepValue = X_SYMBOL_SIZE * div * targetCamera.orthographicSize / textMatrixes[0].fontSize;

            for (int i = 0; ; i++)
            {
                char ch = GetRandomChar();
                if (i > 0 && i % (int)(stepValue + 1) == 0)
                {
                    ch = '\n';
                    endlineCount++;
                }

                _stringBuilder.Append(ch);

                if (endlineCount > targetCamera.orthographicSize / textMatrixes[0].fontSize * 15.5f)
                {
                    break;
                }

                if (i > 999999)
                {
                    Debug.LogError("WTF, almost million symbols!");
                    break;
                }
            }

            RenewAllMatrixText(_stringBuilder.ToString());
        }

        void UpdateMatrix()
        {
            _currentMatrixIndex++;
            if (_currentMatrixIndex >= textMatrixes.Length)
            {
                _currentMatrixIndex = 0;
            }

            for (int i = 0; i < _stringBuilder.Length * regeneratedTextPercent; i++)
            {
                int randomNum = UnityEngine.Random.Range(0, _stringBuilder.Length);
                char currentChar = _stringBuilder[randomNum];

                if (currentChar == ' ' && _currentMatrixIndex != 0) // dont clear empty space often
                {
                    continue;
                }

                if (currentChar != '\n')
                {
                    _stringBuilder[randomNum] = GetRandomChar();
                }
            }

            UpdateLatestMatrix(_stringBuilder.ToString());
        }

        void RenewAllMatrixText (string newText)
        {
            for (int i = 0; i < textMatrixes.Length; i++)
            {
                if (i == _currentMatrixIndex)
                {
                    textMatrixes[i].text = newText;
                } else
                {
                    textMatrixes[i].text = "";
                }
            }
        }

        void UpdateLatestMatrix (string textUpdate)
        {
            textMatrixes[_currentMatrixIndex].text = textUpdate;
        }

        char GetRandomChar()
        {
            int min = 36;
            char ch = (char)(UnityEngine.Random.Range(min, 93));

            if (ch == '<')
            {
                ch = ' ';
            }

            if (UnityEngine.Random.Range(0, 100) < emptySpaceTextPercent)
            {
                ch = ' ';
            }
            return ch;
        }

        Vector2 prevSize;

        private void LateUpdate()
        {
            var curSize = new Vector2(Screen.width, Screen.height);
            if (curSize != prevSize)
            {
                GenerateNewTextMatrix();
                prevSize = curSize;
            }
        }
    }
}

